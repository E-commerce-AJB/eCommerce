$(function() {
  $(window).on("scroll", function() {
      if($(window).scrollTop() > 100) {
          $(".navbar").addClass("active");
      } else {
         $(".navbar").removeClass("active");
      }
  });
});


const cssClass = '.dropdown-item';
$(`${cssClass} a`).on('click', (e) => {
  e.preventDefault();
  const $target = $(e.target);
  const $parent = $target.closest(cssClass);
  const childItemCount = $parent.find('ul li').length;
  
  $parent.toggleClass(`expand-${childItemCount}`);
});
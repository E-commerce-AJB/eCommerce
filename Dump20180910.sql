-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D4E6F819D86650F` (`user_id`),
  CONSTRAINT `FK_D4E6F81A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,1,5,'de Sèze','69006','Lyon'),(2,2,5,'rue des Lianes','69006','Fuenlabrada'),(3,3,5,'rue du paradis','69006','Lyon'),(4,4,5,'rue des Lianes','69006','Lyon');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_64C19C112469DE2` (`category_id`),
  CONSTRAINT `FK_64C19C112469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,NULL,'Hommes'),(2,NULL,'Femmes'),(3,1,'Tee-shirt '),(4,NULL,'Accessoires'),(5,NULL,'Skates'),(6,1,'Sweat-shirt '),(7,1,'Pantalon'),(8,1,'Chaussures '),(9,2,'Tee-shirt '),(10,2,'Sweat-shirt '),(11,2,'Pantalon '),(12,2,'Chaussures '),(13,4,'Sacs'),(14,4,'Casquettes'),(15,4,'Bonnets'),(18,5,'skates'),(19,5,'Roulettes');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180830152755'),('20180831211928'),('20180831212104'),('20180902125656'),('20180902134310'),('20180902164457');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD9777D11E` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,13,'Sac à dos.','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',8,'uploads/c0b4632e523abc1140fbac8fdb9e46b4.png'),(2,13,'Sac à dos.','Sac à dos en coton, trois compartiments et poche zippé sur le devant. Brides règlables.',66,'uploads/3b6ec75d65cadc58f60b0f6f7579666b.png'),(3,12,'Baskets','Baskets en coton, semelle polymère.',73,'uploads/5bc9ca3850a77988b1bc47a77a07d4bd.png'),(4,10,'Sweat-shirt','Sweat-shirt bien joli et tout, avec des man,ches, tout moelleux, tiens chaud en hiver comme en été, du cou, il est déconseillé de le porter en été => rapport à la chaleur.',54,'uploads/cf01785c9e03d7a72df2995050951935.png'),(5,18,'Skate Rose','Skate rose mais unisexe quand même, 2018 quand même!',123,'uploads/0dc140666a47913d9d98edbefcd57270.jpeg'),(6,3,'Tee-shirt très chouette','Tee-shirt col rond, en tissus, bien chouette. 100% coton. Manches courtes, parcequ\'il fait chaud.',45,'uploads/0418995409eafeaba9d1e39a051663b7.png'),(7,8,'Baskets','Bleues avec des petits carreaux genre damier mais sans les pions.',87,'uploads/b6dfa6ff48fad3b7583f49b39ddfb5a6.png'),(8,14,'Casquette','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',57,'uploads/1bb86702d40a7d87686f111283ff0bf3.png'),(9,15,'Bonnet','Sweat-shirt bien joli et tout, avec des man,ches, tout moelleux, tiens chaud en hiver comme en été, du cou, il est déconseillé de le porter en été => rapport à la chaleur.',43,'uploads/21c5b1df925f69c7812f14c9a8369666.png'),(10,6,'Sweat-shirt','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',79,'uploads/395a832e96ddfbe954364c08cddd9c9a.jpeg'),(11,9,'Tee-shirt très chouette','Tee-shirt fait en tissus, bien chouette. 100% coton. Manches courtes, blablabla, et aussi bla bla bli.  J\'aime bien les tee-shirts.',76,'uploads/44f6fc519846c4fa5750c11fc5ad8117.jpeg'),(12,18,'Skate Bleu','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',79,'uploads/54ade23170ef82e8f112301095626feb.jpeg'),(13,3,'Tee-shirt très chouette','Tee-shirt fait en tissus, bien chouette. 100% coton. Manches courtes, blablabla, et aussi bla bla bli.  J\'aime bien les tee-shirts.',67,'uploads/963e362650b189a8ddd6b3bb026c329d.jpeg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_line`
--

DROP TABLE IF EXISTS `product_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopping_cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5CFC9657DE18E50B` (`product_id`,`shopping_cart_id`),
  KEY `IDX_5CFC96574584665A` (`product_id`),
  KEY `IDX_5CFC96573F611409` (`shopping_cart_id`),
  CONSTRAINT `FK_5CFC96574584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_5CFC965745F80CD` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_cart` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_line`
--

LOCK TABLES `product_line` WRITE;
/*!40000 ALTER TABLE `product_line` DISABLE KEYS */;
INSERT INTO `product_line` VALUES (2,1,1,3,8),(6,2,6,2,90),(8,2,5,2,246),(9,2,1,2,16),(10,2,3,2,146),(11,2,2,3,198),(12,2,11,-7,-532),(14,4,4,3,54),(15,4,11,16,1216),(16,4,5,24,2952),(18,5,6,24,1080),(19,5,11,12,228),(24,6,10,7,395),(26,11,4,3,162),(27,11,NULL,0,162),(28,11,6,1,45),(30,12,6,2,45),(31,13,6,1,45),(32,14,11,11,836),(33,14,5,4,492),(34,15,11,4,304),(35,15,5,2,246),(36,16,6,7,315),(37,21,10,15,1185),(38,21,5,16,1968),(39,22,6,17,765),(40,23,8,21,1197),(41,24,5,3,369),(42,25,5,1,123),(45,26,5,5,615),(46,26,1,1,8),(47,27,6,2,90),(48,28,11,4,304),(49,28,5,2,246),(50,29,5,1,123),(51,30,6,3,135),(52,30,4,2,108),(53,32,7,1,87),(62,36,6,15,675),(70,37,12,2,158),(71,37,5,1,123),(72,37,11,1,76),(73,37,3,1,73),(77,39,6,1,45),(78,40,8,1,57);
/*!40000 ALTER TABLE `product_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `purchase` tinyint(1) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_72AAD4F6A76ED395` (`user_id`),
  CONSTRAINT `FK_72AAD4F6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (1,1,1,NULL),(2,1,1,NULL),(3,NULL,0,NULL),(4,1,1,NULL),(5,1,1,NULL),(6,1,NULL,NULL),(7,NULL,NULL,NULL),(8,NULL,NULL,NULL),(9,NULL,NULL,NULL),(10,NULL,NULL,NULL),(11,2,NULL,NULL),(12,1,NULL,NULL),(13,1,NULL,NULL),(14,1,1,NULL),(15,1,1,NULL),(16,1,1,NULL),(17,NULL,1,NULL),(18,NULL,1,NULL),(19,NULL,1,NULL),(20,NULL,1,NULL),(21,1,1,NULL),(22,1,1,NULL),(23,1,1,NULL),(24,1,1,NULL),(25,1,1,NULL),(26,1,0,NULL),(27,1,1,NULL),(28,1,1,NULL),(29,1,0,NULL),(30,1,1,NULL),(31,NULL,0,NULL),(32,1,1,NULL),(33,NULL,0,NULL),(34,1,0,NULL),(35,1,0,NULL),(36,1,1,NULL),(37,1,1,437),(38,NULL,0,0),(39,1,1,52),(40,1,1,64),(41,NULL,0,0);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_role` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Berta','munoz','berta@berta.com','$2y$12$TJVDtdBnSfSW2m3UraxRdu4Hbcxt75MQS/lxzOOmAzr4N9qbL6h12','0644204588',1),(2,'Titi','GrosMinet','titi@grosminet.com','$2y$12$AswNxCzernq4tzEO1esXOu.vo8gcWGEeaFg9Ris7jk5/ooJn0KA8m','0644204588',0),(3,'lkjbljb','De Nazareth','bertiu','$2y$12$8W8yNIIGooebAG6j.OjRKug0ORjEjapMX8B5/m8SSBwkmp/d4wI6S','080808080808',0),(4,'lkjbljb','De Nazareth','jesus@satan.com','$2y$12$CCyazgBouuSSoCsXncY/L.mQGgpPIsc.fk8wJ60osR7gOBhYo2zBe','0644204588',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 16:46:54

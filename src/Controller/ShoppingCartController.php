<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;
use App\Repository\ShoppingCartRepository;
use Symfony\Component\HttpKernel\Tests\EventListener\DumpListenerTest;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\UserRepository;
use App\Entity\Product;
use App\Entity\ProductLine;



class ShoppingCartController extends Controller
{
    //Panier d'achats//
    /**
     * @Route("user/shopping/cart", name="shopping_cart")
     */
    public function index(Session $session, ObjectManager $manager, ShoppingCartRepository $repo)
    {
        $cart = $session->get("cart");

        if (!$cart) {

            $cart = new ShoppingCart();
        }
        $cart->setPurchase(false);

        $sousTotalPrice = 0;
        $total = 0;

        foreach ($cart->getProductLine() as $line) {

            $sousTotalPrice = $sousTotalPrice + $line->getPrice();
            $total = $sousTotalPrice + 7;
        }
        $cart = $this->getDoctrine()->getManager()->merge($cart);
        $productLine = $cart->getProductLine();
        $cart->setTotal($total);
        $manager->persist($cart);
        $manager->flush();
        $session->set("cart", $cart);

        return $this->render('shopping_cart/index.html.twig', [
            "cart" => $cart,
            "sousTotalPrice" => $sousTotalPrice,
            "total" => $total
        ]);

    }
    //Récapitulatif de la commande//
    /**
     * @Route("user/orderConfirmation", name="confirm")
     */
    public function confirmation(Session $session, ObjectManager $manager)
    {

        dump($session->get("cart"));
        $cart = $session->get("cart");
        if (!$cart) {

            $cart = new ShoppingCart();
        }

        $sousTotalPrice = 0;
        $total = 0;

        foreach ($cart->getProductLine() as $line) {

            $sousTotalPrice = $sousTotalPrice + $line->getPrice();
            $total = $sousTotalPrice + 7;
        }
        $cart = $this->getDoctrine()->getManager()->merge($cart);
        $productLine = $cart->getProductLine();


        $manager->persist($cart);
        $manager->flush();
        $session->set("cart", $cart);

        $repo = $this->getDoctrine()->getRepository(User::class);


        return $this->render('shopping_cart/order.html.twig', [
            "cart" => $cart,
            "sousTotalPrice" => $sousTotalPrice,
            "total" => $total
        ]);

    }

        //Validation de la commande, passe le panier au statut de commande et vide le panier //
    /**
     * @Route("user/cartValidation", name = "cartValidation")
     */
    public function cartValidation(ObjectManager $manager, Session $session)
    {
        $shoppingCart = $session->get("cart");
        if (!$shoppingCart) {

            $shoppingCart = new ShoppingCart();
        }

        $shoppingCart = $this->getDoctrine()->getManager()->merge($shoppingCart);
        $shoppingCart->setPurchase(true);
        $manager->persist($shoppingCart);
        $manager->flush();
        $session->remove("cart", $shoppingCart);

        return $this->redirectToRoute('shopping_cart');

        return $this->render(
            'base.html.twig',
            []
        );

    }
      //Affichage de l'historique d'achat du client//
    /**
     * @Route("User/history", name = "history")
     */
    public function history(ShoppingCartRepository $repo)
    {

        $repo = $this->getdoctrine()->getRepository(ShoppingCart::class); 

        return $this->render('compte/history.html.twig', [
            
            'shoppingCart'=> $repo->findAll()
        ]);
       
    }

      //Affichage des commandes validées (par id)//
    /**
     * @Route("User/showOrder/{id}", name = "showOrder")
     */
    public function showOrder(ShoppingCartRepository $repo, Int $id/*, ProductLine $productLine*/)
    {
        $repo = $this->getdoctrine()->getRepository(ShoppingCart::class); 
        $result = $repo->find($id);

        return $this->render('compte/showOrder.html.twig', [
            'cart'=> $result,
            'productLine'=> $id,

        ]);
    }  
   
}
  


<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;


class HomeController extends Controller
{
    //Page d'accueil//
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $repo = $this->getdoctrine()->getRepository(Category::class); 
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'category'=> $repo->findBy(["category"=>null])
        ]);
    }

    // affiche les tous produits dans la page admin//
     /**
     * @Route("/admin", name="admin")
     */
    public function articles(ProductRepository $repo)
    {
        $repo = $this->getdoctrine()->getRepository(Product::class); 

        return $this->render('admin/index.html.twig', [
            
            'product'=> $repo->findAll()
        ]);
    }


    //affiche un produit seul via son id)//

    /**
     * @Route("/articleSeul/{id}", name="single")
     */
    public function singleProduct(string $id, ProductRepository $repo, Request $request, Product $product)
    {

        if ($request->isMethod('POST')){
            $number = $request->get("number");

            return $this->redirectToRoute("product_line", ["id"=>$product->getId(), "number"=>$number]);
        }

        $result = $repo->find($id);
        return $this->render('single_Product/index.html.twig', [
            'product'=> $result
        ]);
    }

    //affiche les articles par catégories et sous-catégories depuis les liens de la page d'accueil
    /**
     * @Route("/categories/{id}", name="categories")
     */ 

    public function affichCat(Category $id)
    {
        $repo = $this->getdoctrine()->getRepository(Product::class); 
        return $this->render('ProductByCategory/index.html.twig', [
            
            'product'=> $this->recursiveProduct($id),
            'category' => $id
        ]);
    }

    private function recursiveProduct(Category $category) {

        $products = $category->getProducts();

        foreach ($category->getSubCategories() as $sub){
            $products = new ArrayCollection(array_merge($products->toArray(), $this->recursiveProduct($sub)->toArray()));
            
        }

        return $products;
        
    }
}




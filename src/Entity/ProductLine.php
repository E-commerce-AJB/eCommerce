<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductLine
 *
 * @ORM\Table(name="product_line", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_5CFC9657DE18E50B", columns={"product_id", "shopping_cart_id"})}, indexes={@ORM\Index(name="IDX_5CFC96573F611409", columns={"shopping_cart_id"})})
 * @ORM\Entity
 */
class ProductLine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var \ShoppingCart
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ShoppingCart", inversedBy="productLine")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shopping_cart_id", referencedColumnName="id")
     * })
     */
    private $shoppingCart;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getShoppingCart(): ?ShoppingCart
    {
        return $this->shoppingCart;
    }

    public function setShoppingCart(?ShoppingCart $shoppingCart): self
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    
    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

}
